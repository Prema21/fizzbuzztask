﻿using FizzBuzzRules.Interfaces;
using FizzBuzzRules.Rules;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace FizzBuzzRulesTests
{
    [TestClass]
    public class DivisibleByFiveRuleTests
    {
        private Mock<IDayOfTheWeekRule> dayOfTheWeekRuleMock;

        [TestInitialize]
        public void TestSetup()
        {
            this.dayOfTheWeekRuleMock = new Mock<IDayOfTheWeekRule>();
        }


        [TestMethod]
        [DataRow(50, true)]
        [DataRow(20, true)]
        [DataRow(1500, true)]
        [DataRow(17, false)]
        [DataRow(99999, false)]
        public void When_NumberIsDivisibleByFive_Should_Retun_Result(int userInput, bool expectedResult)
        {
            // Arrange
            var divisibleByFiveRule = new DivisibleByFiveRule(this.dayOfTheWeekRuleMock.Object);

            // Act

            var result = divisibleByFiveRule.CheckHandler(userInput);

            // Assert
            result.Should().Be(expectedResult);

        }

        [TestMethod]
        [DataRow(true, "Wuzz")]
        [DataRow(false, "Buzz")]
        public void When_TheDayIsWenesdayAndNumberIsDivisibleByFive_Should_Retun_Result(bool value, string expectedValue)
        {
            // Arrange
            var divisibleByFiveRule = new DivisibleByFiveRule(this.dayOfTheWeekRuleMock.Object);
            this.dayOfTheWeekRuleMock.Setup(x => x.CheckHandler(DateTime.Now.DayOfWeek)).Returns(value);

            // Act

            var result = divisibleByFiveRule.GetResult();

            // Assert
            result.Should().BeEquivalentTo(expectedValue);
        }
    }
}
