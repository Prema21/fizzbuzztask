﻿using FizzBuzzRules.Interfaces;
using FizzBuzzRules.Rules;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace FizzBuzzRulesTests
{
    [TestClass]
    public class DivisibleByThreeRulesTests
    {
        [TestClass]
        public class DivisibleByThreeRuleTests
        {
            private Mock<IDayOfTheWeekRule> dayOfTheWeekRuleMock;

            [TestInitialize]

            public void TestSetup()
            {
                this.dayOfTheWeekRuleMock = new Mock<IDayOfTheWeekRule>();
            }

            [TestMethod]
            [DataRow(33, true)]
            [DataRow(999, true)]
            [DataRow(24, true)]
            [DataRow(100, false)]
            [DataRow(2355, true)]

            public void When_NumberIsDivisibleByThree_Should_Retun_Result(int userInput, bool expectedResult)
            {
                // Arrange
                var divisibleByThreeRule = new DivisibleByThreeRule(this.dayOfTheWeekRuleMock.Object);

                // Act

                var result = divisibleByThreeRule.CheckHandler(userInput);

                // Assert
                result.Should().Be(expectedResult);

            }

            [TestMethod]
            [DataRow(true, "Wizz")]
            [DataRow(false, "Fizz")]

            public void When_TheDayIsWenesdayAndNumberIsDivisibleByThree_Should_Retun_Result(bool value, string expectedValue)
            {
                // Arrange
                var divisibleByThreeRule = new DivisibleByThreeRule(this.dayOfTheWeekRuleMock.Object);
                this.dayOfTheWeekRuleMock.Setup(x => x.CheckHandler(DateTime.Now.DayOfWeek)).Returns(value);

                // Act

                var result = divisibleByThreeRule.GetResult();

                // Assert
                result.Should().BeEquivalentTo(expectedValue);

            }
        }
    }
}
