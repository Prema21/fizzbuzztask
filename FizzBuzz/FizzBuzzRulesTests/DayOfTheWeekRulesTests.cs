﻿namespace FizzBuzzRulesTests
{
    using FizzBuzzRules.Rules;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;

    [TestClass]
    public class DayOfTheWeekRulesTests
    {
        [TestMethod]
        public void When_TheDayIsWenesday_CheckHandler_Should_Return_True()
        {
            //Arrange

            var dayOfTheWeekRule = new DayOfTheWeekRule();

            //Act

            var result = dayOfTheWeekRule.CheckHandler(DayOfWeek.Wednesday);

            //Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public void When_TheDayIsNotWenesday_CheckHandler_Should_Return_False()
        {
            //Arrange

            var dayOfTheWeekRule = new DayOfTheWeekRule();

            //Act

            var result = dayOfTheWeekRule.CheckHandler(DayOfWeek.Thursday);

            //Assert
            result.Should().BeFalse();
        }

    }
}

