﻿using FizzBuzzRules.Interfaces;
using FizzBuzzRules.Rules;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FizzBuzzRulesTests
{
    [TestClass]
        public class FizzBuzzRepositoryTests
        {
            private Mock<IRules> divisibleByThreeMock;
            private Mock<IRules> divisibleByFiveMock;
            private FizzBuzzRepository fizzBuzzRepository;

            [TestInitialize]

            public void TestSetup()
            {
                this.divisibleByThreeMock = new Mock<IRules>();
                this.divisibleByThreeMock.Setup(x => x.CheckHandler(It.Is<int>(i => i % 3 == 0))).Returns(true);
                this.divisibleByThreeMock.Setup(x => x.GetResult()).Returns("Fizz");


                this.divisibleByFiveMock = new Mock<IRules>();
                this.divisibleByFiveMock.Setup(x => x.CheckHandler(It.Is<int>(i => i % 5 == 0))).Returns(true);
                this.divisibleByFiveMock.Setup(x => x.GetResult()).Returns("Buzz");

                this.fizzBuzzRepository = new FizzBuzzRepository(new[]
                {
                this.divisibleByThreeMock.Object,
                this.divisibleByFiveMock.Object,
            });
            }

            [TestMethod]
            public void GetResults_Should_Return_FizzBuzzList()
            {
                // Arrange, Act
                var result = this.fizzBuzzRepository.GetResults(5);

                // Assert

                result.Count.Should().Be(5);
                result[0].Should().BeEquivalentTo("1");
                result[1].Should().BeEquivalentTo("2");
                result[2].Should().BeEquivalentTo("Fizz");
                result[3].Should().BeEquivalentTo("4");
                result[4].Should().BeEquivalentTo("Buzz");

            }
        }
    }
