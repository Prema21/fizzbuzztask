﻿namespace FizzBuzz.Controllers
{
    using System;
    using System.Web.Mvc;
    using FizzBuzz.Models;
    using FizzBuzzRules.Interfaces;
    using PagedList;

    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzRepository FizzBuzzRepository;
        private readonly int pageSize = 20;
        private int pageNumber;
        public FizzBuzzController(IFizzBuzzRepository FizzBuzzRepository)
        {
            this.FizzBuzzRepository = FizzBuzzRepository;
        }

        [HttpGet]
        public ActionResult Index(string inputNumber, int? page)
        {
            var model = new FizzBuzzModel();

            if (!string.IsNullOrEmpty(inputNumber))
            {
                int number = Convert.ToInt32(inputNumber);
                model = this.GetFizzBuzzResult(number, page);
                return View("Index", model);
            }

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult Index(FizzBuzzModel model, int? page)
        {
            var fizzbuzzModel = new FizzBuzzModel();
            if (ModelState.IsValid)
            {
                int number = Convert.ToInt32(model.InputNumber);
                fizzbuzzModel = this.GetFizzBuzzResult(number, page);
                return View("Index", fizzbuzzModel);
            }
            return View("Index", fizzbuzzModel);
        }

        private FizzBuzzModel GetFizzBuzzResult(int userInput, int? page)
        {
            pageNumber = page.HasValue ? page.Value : 1;

            var result = this.FizzBuzzRepository.GetResults(userInput);
            var model = new Models.FizzBuzzModel
            {
                InputNumber = userInput,
                numbers = (PagedList<string>)result.ToPagedList(pageNumber, pageSize)
            };
            return model;
        }
    }
}
