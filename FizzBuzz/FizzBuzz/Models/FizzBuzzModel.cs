﻿namespace FizzBuzz.Models
{
    using PagedList;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class FizzBuzzModel
    {

        [Required(ErrorMessage = "Please Enter a Number")]
        [Range(1, 1000, ErrorMessage = "Please Enter a Number between 1 and 1000")]
        [Display(Name = "User Input")]
        public int? InputNumber { get; set; }

        public IPagedList<string> numbers { get; set; }
    }
}
