﻿namespace FizzBuzzTests
{
    using FizzBuzz.Controllers;
    using FizzBuzzRules.Interfaces;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System.Collections.Generic;
    using System.Web.Mvc;

    [TestClass]
    public class FizzBuzzControllerTests
    {
        private Mock<IFizzBuzzRepository> fizzBuzzRepositoryMock;
        private FizzBuzzController controller;


        [TestInitialize]

        public void TestSetup()
        {
            this.fizzBuzzRepositoryMock = new Mock<IFizzBuzzRepository>();
            this.controller = new FizzBuzzController(this.fizzBuzzRepositoryMock.Object);
        }

        [TestMethod]
        public void WhenThePageIsLoaded_Index_ShouldReturn_View()
        {
            // Arrange, Act
            var result = this.controller.Index(string.Empty, 1) as ViewResult;

            // Asser
            result.Should().NotBeNull();
            result.ViewName.Should().BeEquivalentTo("Index");
        }

        [TestMethod]
        public void WhenTheUserEnterdValue_Index_ShouldReturn_FizzBuzzList()
        {
            // Arrange
            var mockFizzBuzzList = new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz", "16", "17", "Fizz", "19", "Buzz", "Fizz", "22", "23", "24", "Buzz" };
            this.fizzBuzzRepositoryMock.Setup(x => x.GetResults(25)).Returns(mockFizzBuzzList);

            // Act
            var result = this.controller.Index("25", 1) as ViewResult;

            // Assert
            result.Should().NotBeNull();
            var model = result.ViewData.Model as FizzBuzz.Models.FizzBuzzModel;
            model.numbers[0].Should().BeEquivalentTo("1");
            model.numbers[9].Should().BeEquivalentTo("Buzz");
        }

        [TestMethod]
        public void WhenTheUserClickedNextButton_Index_ShouldReturn_FizzBuzzList()
        {
            // Arrange
            var mockFizzBuzzList = new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz", "16", "17", "Fizz", "19", "Buzz", "Fizz", "22", "23", "24", "Buzz" };
            this.fizzBuzzRepositoryMock.Setup(x => x.GetResults(25)).Returns(mockFizzBuzzList);

            // Act
            var result = this.controller.Index("25", 2) as ViewResult;

            // Asser

            result.Should().NotBeNull();
            var model = result.ViewData.Model as FizzBuzz.Models.FizzBuzzModel;
            model.numbers[0].Should().BeEquivalentTo("Fizz");
            model.numbers[4].Should().BeEquivalentTo("Buzz");
        }
    }
}
