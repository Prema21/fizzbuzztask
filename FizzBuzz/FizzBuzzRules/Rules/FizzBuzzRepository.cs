﻿using FizzBuzzRules.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzRules.Rules
{
    public class FizzBuzzRepository : IFizzBuzzRepository
    {
        private readonly IRules[] rules;
        public FizzBuzzRepository(IRules[] rules)
        {
            this.rules = rules;
        }
        public List<string> GetResults(int? number)
        {
            var numbers = new List<string>();
            for (int i = 1; i <= number; i++)
            {
                var matchedNumberHandler = this.rules.Where(x => x.CheckHandler(i));

                if (matchedNumberHandler.Any())
                {
                    var list = string.Join(" ", matchedNumberHandler.Select(x => x.GetResult()));

                    numbers.Add(list);
                }
                else
                {
                    numbers.Add(i.ToString());
                }
            }
            return numbers;
        }
    }
}
 
