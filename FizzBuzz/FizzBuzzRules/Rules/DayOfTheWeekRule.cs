﻿using FizzBuzzRules.Interfaces;
using System;

namespace FizzBuzzRules.Rules
{
    public class DayOfTheWeekRule : IDayOfTheWeekRule
    {
        public bool CheckHandler(DayOfWeek dayOfWeek)
        {
            return dayOfWeek == DayOfWeek.Wednesday;
        }
    }
}
