﻿using FizzBuzzRules.Interfaces;
using System;

namespace FizzBuzzRules.Rules
{
    public class DivisibleByFiveRule : IRules
    {
        private readonly IDayOfTheWeekRule dayOfTheWeekRule;
        public DivisibleByFiveRule(IDayOfTheWeekRule dayOfTheWeekRule)
        {
            this.dayOfTheWeekRule = dayOfTheWeekRule;
        }
        public bool CheckHandler(int number)
        {
            return number % 5 == 0;
        }

        public string GetResult()
        {
            return this.dayOfTheWeekRule.CheckHandler(DateTime.Now.DayOfWeek) ? "Wuzz" : "Buzz";
        }
    }
}
