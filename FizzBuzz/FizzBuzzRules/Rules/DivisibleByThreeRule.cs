﻿using FizzBuzzRules.Interfaces;
using System;

namespace FizzBuzzRules.Rules
{
    public class DivisibleByThreeRule : IRules 
    {
        private readonly IDayOfTheWeekRule dayOfTheWeekRule;
        public DivisibleByThreeRule(IDayOfTheWeekRule dayOfTheWeekRule)
        {
            this.dayOfTheWeekRule = dayOfTheWeekRule;
        }
        public bool CheckHandler(int number)
        {
            return number % 3 == 0;
        }
        
        public string GetResult()
        {
            return this.dayOfTheWeekRule.CheckHandler(DateTime.Now.DayOfWeek) ? "Wizz" : "Fizz";
        }
    }
}
