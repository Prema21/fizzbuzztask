﻿using System.Collections.Generic;

namespace FizzBuzzRules.Interfaces
{
    public interface IFizzBuzzRepository
    {
        List<string> GetResults(int? number);
    }
}
