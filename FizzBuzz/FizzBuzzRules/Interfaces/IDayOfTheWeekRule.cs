﻿using System;

namespace FizzBuzzRules.Interfaces
{
    public interface IDayOfTheWeekRule
    {
        bool CheckHandler(DayOfWeek dayOfWeek);
    }
}
